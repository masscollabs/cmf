# cmf

convergents maintainers framework is a framework that handles Git workflow between different forges and their infrastructures.

And also we plan a portal to analyse the code , git workflow and more...

And also from now on it is a replacement for GTK+ and Qt and similar technologies !...

# License

convergents maintainers framework 

Copyright (C) 2024-2025 QB Networks

Copyright (C) 2017-2025 Masscollabs Services

Copyright (C) 2017-2025 Procyberian and contributors

Copyright (C) 2017-2025 Mass Collaboration Labs and contributors

Copyright (C) 2017-2025 amassivus and contributors

Copyright (C) 2024-2025 godigitalist and contributors

Copyright (C) 2024-2025 bilsege and contributors

Copyright (C) 2024-2025 Birleşik Dergi Yazarları

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.

# Document and artwork license

[CC BY-SA 4.0 or later](by-sa.markdown)
